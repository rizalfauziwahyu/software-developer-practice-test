const jwt = require("jsonwebtoken");
const secret = process.env.JWT_SECRET;
const db = require("../_helpers/db");

module.exports.authorize = (roles = []) => {
  return async (req, res, next) => {
    try {
      // roles param can be a single role string (e.g. Role.User or 'User')
      // or an array of roles (e.g. [Role.board, Role.expert] or ['board', 'expert'])
      if (typeof roles == "string") {
        roles = [roles];
      }

      let token;
      const authHeader = req.get("Authorization");
      if (!authHeader) {
        if (req.cookies.token) {
          token = req.cookies.token.split(" ")[1];
        } else {
          const error = new Error("Unauthorized user");
          error.statusCode = 401;
          throw error;
        }
      }

      if (!token) {
        token = authHeader.split(" ")[1];
      }

      const decodedToken = jwt.verify(token, secret);

      if (!decodedToken) {
        const error = new Error("Unauthorized user");
        error.statusCode = 401;
        throw error;
      }

      const user = await db.User.findById(decodedToken.id);

      if (!user || (roles.length && !roles.includes(user.profile))) {
        // user no longer exists or role not authorized
        const error = new Error("Unauthorized user");
        error.statusCode = 401;
        throw error;
      }
      // authentication and authorization successful
      req.user = user;
      const refreshTokens = await db.RefreshToken.find({ user: user._id });
      req.user.ownsToken = (token) =>
        !!refreshTokens.find((x) => x.token === token);

      next();
    } catch (err) {
      if (!err.statusCode) {
        if (err.message == "jwt expired") {
          err.message = "Unauthorized user";
          err.statusCode = 401;
        } else {
          err.statusCode = 500;
        }
      }
      next(err);
    }
  };
};
