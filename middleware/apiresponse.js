module.exports.getResponse = (success, message, data) => {
  return {
    success: success,
    message: message,
    data: data,
  };
};

module.exports.errorHandler = (error, req, res, next) => {
  let status = error.statusCode;
  const message = error.message;
  const data = error.data;
  const errorCode = error.statusCode;

  if (!status) status = 500;

  res.status(status).json({
    success: false,
    message: message,
    error_code: errorCode,
    data: data,
  });
};

