# Software Developer Practical Test.<br>

### Framework
#### [ReactJs](https://reactjs.org/)
A JavaScript library for building user interfaces

#### [Redux](https://redux.js.org/introduction/getting-started)
Redux is a predictable state container for JavaScript apps.

#### [Express](https://docs.nestjs.com/)
Fast, unopinionated, minimalist web framework for Node.js

### Database
#### [Mongodb](https://mongodb.com)
Mongodb is general purpose, document based, distributed database built for modern application developers and for the cloud era.

### Authentication
#### [JWT](https://jwt.io/)
JWT works best for single use tokens. Ideally, a new JWT must be generated for each use.

### API Documentation
#### [POSTMAN Documentation](https://documenter.getpostman.com/view/6320297/T1LTdPTM?version=latest)

## Get Started

### Precondition
* Node.js (> = 8.9.0)
* Postman

#### <i>Clone</i> Project App

git

```bash
https://gitlab.com/rizalfauziwahyu/software-developer-practice-test.git
```

#### <i> Download dependencies </i>

use [npm](https://www.npmjs.com/) that have installed on your pc.

```bash
npm install
```


#### Running App

Running backend in development mode port 5000.
```bash
npm run start:dev
```

Running frontend inside client directory, default port 3000.
```bash
cd ./client
npm start
```

### Account Information

#### Board profile
username: board
password: password

#### Expert profile
username: expert
password: password

#### Trainer profile
username: trainer
password: trainer

### Screenshots App
##### Login
![Screen Shot](documentation/loginpage.PNG)

##### Homepage trainer profile
![Screen Shot](documentation/homepage_trainer.PNG)

##### Homepage expert profile
![Screen Shot](documentation/homepage_experts.PNG)

##### Register an activity only expert
![Screen Shot](documentation/registeractivity_experts.PNG)

##### Update an activity only expert
![Screen Shot](documentation/updateactivity_experts.PNG)

##### Homepage board profile
![Screen Shot](documentation/homepage_board.PNG)



## Contributors
* Muhammad Rizal Fauzi Wahyu







