const mongoose = require("mongoose");
const connectionOptions = {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
};
const MONGODB_URI = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@myfirstcluster-oqz6j.gcp.mongodb.net/${process.env.MONGO_DEFAULT_DATABASE}?retryWrites=true&w=majority`;
const MONGODB_LOCAL_URI = `${process.env.MONGODB_URI}`;
mongoose
  .connect(MONGODB_URI, connectionOptions)
  .then(() => {
    console.log("connected to mongodb");
  })
  .catch((err) => {
    console.log(err);
  });
mongoose.Promise = global.Promise;

module.exports = {
  User: require("../models/User"),
  RefreshToken: require("../models/RefreshToken"),
  Skill: require('../models/Skill'),
  TrainingActivity: require('../models/TrainingActivity'),
  isValidId,
};

function isValidId(id) {
  return mongoose.Types.ObjectId.isValid(id);
}
