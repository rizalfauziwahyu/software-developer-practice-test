module.exports = errorHandler;

function errorHandler(error, req, res, next) {
  const status = error.statusCode;
  const message = error.message;
  const data = error.data;

  res.status(status).json({
    success: false,
    message: message,
    data: data,
  });
}
