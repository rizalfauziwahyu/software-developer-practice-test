const bcrypt = require('bcryptjs');
const db = require('./db');

module.exports = seeds;

async function seeds() {
  // seed skills
  if ((await db.Skill.countDocuments()) === 0) {
    const skills = await db.Skill.insertMany([
      {
        skill_id: '1111',
        skill_name: 'Architectural Stonemasonry',
      },
      {
        skill_id: '1111',
        skill_name: 'Carpentry',
      },
      {
        skill_id: '1111',
        skill_name: 'Joinery',
      },
      {
        skill_id: '1111',
        skill_name: 'Electrical Installations',
      },
      {
        skill_id: '1111',
        skill_name: 'Painting and Decorating',
      },
      {
        skill_id: '1111',
        skill_name: 'Concrete Construction Work',
      },
      {
        skill_id: '2222',
        skill_name: '3D Digital Game Art',
      },
      {
        skill_id: '2222',
        skill_name: 'Graphic Design Technology',
      },
      {
        skill_id: '2222',
        skill_name: 'Fashion Technology',
      },
      {
        skill_id: '2222',
        skill_name: 'Jewellery',
      },
      {
        skill_id: '2222',
        skill_name: 'Floristry',
      },
      {
        skill_id: '2222',
        skill_name: 'Visual Merchandising',
      },
      {
        skill_id: '3333',
        skill_name: 'Cloud Computing',
      },
      {
        skill_id: '3333',
        skill_name: 'IT Network Systems Administration',
      },
      {
        skill_id: '3333',
        skill_name: 'Web Technologies',
      },
      {
        skill_id: '3333',
        skill_name: 'Cyber Security',
      },
      {
        skill_id: '3333',
        skill_name: 'IT Software Solutions for Business',
      },
      {
        skill_id: '3333',
        skill_name: 'Information Network Cabling',
      },
    ]);
    console.log('skills seed successful');
  }
  // seed board user account
  if ((await db.User.countDocuments()) === 0) {
    const boardAccount = new db.User({
      email: 'board@practicaltest.com',
      password: bcrypt.hashSync('password'),
      name: 'board',
      username: 'board',
      profile: 'board',
    });
    await boardAccount.save();
    console.log('board account seed successful')
  }
}
