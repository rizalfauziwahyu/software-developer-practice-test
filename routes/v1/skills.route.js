const express = require('express');
const router = express.Router();
const { authorize } = require('../../middleware/authorize');
const skillController = require('../../controllers/skill.controller');

router.get('/', authorize(), skillController.getSkills);

module.exports = router;
