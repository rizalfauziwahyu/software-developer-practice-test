const express = require('express');
const router = express.Router();
const { body } = require('express-validator');
const { authorize } = require('../../middleware/authorize');
const accountController = require('../../controllers/account.controller');
const Role = require('../../_helpers/profile');

router.get('/', authorize(), accountController.getUser);

router.post(
  '/',
  authorize(Role.board),
  [
    body('name').isString().notEmpty(),
    body('email').isEmail(),
    body('username').isString().notEmpty(),
    body('password').trim().isLength({ min: 4 }),
    body('profile').isString().notEmpty(),
  ],
  accountController.register
);

module.exports = router;
