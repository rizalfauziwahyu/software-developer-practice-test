const express = require('express');
const router = express.Router();
const apiResponse = require('../../middleware/apiresponse');

router.get('/csrf-token', (req, res, next) => {
    try {
        const message = 'get csrf token successfully!';
        const data = {
            csrfToken: req.csrfToken(),
        };
        const response = apiResponse.getResponse(true, message, data);

        return res.status(200).json(response);
    } catch (err) {
        if (!err.statusCode) err.statusCode = 500;
        next(err);
    }
});

module.exports = router;