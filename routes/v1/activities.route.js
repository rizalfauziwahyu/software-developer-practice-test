const express = require('express');
const router = express.Router();
const { authorize } = require('../../middleware/authorize');
const activityController = require('../../controllers/trainingactivity.controller');

router.get('/:skill_id', authorize(), activityController.getActivities);

module.exports = router;
