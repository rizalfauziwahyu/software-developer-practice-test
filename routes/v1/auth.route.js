const express = require('express');
const { body } = require('express-validator');
const router = express.Router();
const { authorize } = require('../../middleware/authorize');
const profile = require('../../_helpers/profile');
const authController = require('../../controllers/auth.controller');

router.post(
  '/login',
  [
    body('username').isString().notEmpty(),
    body('password').notEmpty().trim().isLength({ min: 4 }),
  ],
  authController.login
);

router.post('/refresh-token', authController.refreshToken);

router.get('/logout', authorize(), authController.revokeToken);

module.exports = router;
