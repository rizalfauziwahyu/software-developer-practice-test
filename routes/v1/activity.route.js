const express = require('express');
const { body } = require('express-validator');
const router = express.Router();
const { authorize } = require('../../middleware/authorize');
const activityController = require('../../controllers/trainingactivity.controller');
const Role = require('../../_helpers/profile');

router.get('/:id', authorize(), activityController.getActivity);

router.post(
  '/',
  authorize(Role.expert),
  [
    body('skill').isString().notEmpty(),
    body('title').isString().notEmpty(),
    body('description').isString().notEmpty(),
    body('startdate').notEmpty(),
    body('enddate').notEmpty(),
    body('participants').isArray(),
  ],
  activityController.create
);

router.put(
  '/:id',
  authorize(Role.expert),
  [
    body('skill').isString().notEmpty(),
    body('title').isString().notEmpty(),
    body('description').isString().notEmpty(),
    body('startdate').isString().notEmpty(),
    body('enddate').isString().notEmpty(),
    body('participants').isArray(),
  ],
  activityController.update
);

router.delete('/:id', authorize(Role.expert), activityController.destroy);

module.exports = router;
