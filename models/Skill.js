const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  skill_id: {
    type: String,
    required: true,
  },
  skill_name: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('Skill', schema);
