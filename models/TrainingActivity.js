const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    skill_id: {
      type: String,
      required: true,
    },
    skill_name: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    startdate: {
      type: Date,
      required: true,
    },
    enddate: {
      type: Date,
      required: true,
    },
    participants: [
      {
        userId: {
          type: Schema.Types.ObjectId,
          ref: 'User',
        },
      },
    ],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('TrainingActivity', schema);
