const { validationResult } = require('express-validator');
const userServices = require('../services/user.service');
const { getResponse } = require('../middleware/apiresponse');

module.exports = {
  getUser,
  register,
};

function getUser(req, res, next) {
  const response = getResponse(true, 'get user success', req.user);
  return res.status(200).json(response);
}

function register(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error('Data cannot be processed');
    error.statusCode = 422;
    return next(error);
  }

  console.log(req.body);

  // check if user exists
  userServices
    .register(req.body)
    .then(() => {
      const response = getResponse(true, 'create success', null);
      return res.status(200).json(response);
    })
    .catch((err) => {
      if (!err.statusCode) err.statusCode = 500;
      next(err);
    });
}
