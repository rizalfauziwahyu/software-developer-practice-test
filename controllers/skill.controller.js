const skillServices = require('../services/skill.service');
const { getResponse } = require('../middleware/apiresponse');

module.exports = {
  getSkills,
};

function getSkills(req, res, next) {
  skillServices
    .getSkills()
    .then((skills) => {
      const response = getResponse(true, 'get skills success', skills);
      return res.status(200).json(response);
    })
    .catch((err) => {
      if (!err.statusCode) err.statusCode = 500;
      next(err);
    });
}
