const activityServices = require('../services/trainingactivity.service');
const { validationResult } = require('express-validator');
const apiResponse = require('../middleware/apiresponse');

module.exports = {
  getActivities,
  getActivity,
  create,
  update,
  destroy,
};

function getActivities(req, res, next) {
  activityServices
    .getActivities(req.params.skill_id)
    .then((activities) => {
      const response = apiResponse.getResponse(
        true,
        'get activities success',
        activities
      );
      return res.status(200).json(response);
    })
    .catch((err) => {
      if (!err.statusCode) err.statusCode = 500;
      next(err);
    });
}

function getActivity(req, res, next) {
  activityServices
    .getActivity(req.params.id)
    .then((activity) => {
      const response = apiResponse.getResponse(
        true,
        'get activity success',
        activity
      );
      return res.status(200).json(response);
    })
    .catch((err) => {
      if (!err.statusCode) err.statusCode = 500;
      next(err);
    });
}

function create(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error('Data cannot be processed');
    error.statusCode = 422;

    console.log(errors.array());
    return next(error);
  }
  console.log(req.body);
  activityServices
    .create(req.body)
    .then(() => {
      const response = apiResponse.getResponse(true, 'create success', null);
      return res.status(200).json(response);
    })
    .catch((err) => {
      if (!err.statusCode) err.statusCode = 500;
      next(err);
    });
}

function update(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error('Data cannot be processed');
    error.statusCode = 422;
    return next(error);
  }

  activityServices
    .update(req.params.id, req.body)
    .then(() => {
      const response = apiResponse.getResponse(true, 'update success', null);
      return res.status(200).json(response);
    })
    .catch((err) => {
      if (!err.statusCode) err.statusCode = 500;
      next(err);
    });
}

function destroy(req, res, next) {
  activityServices
    .destroy(req.params.id)
    .then(() => {
      const response = apiResponse.getResponse(true, 'delete success', null);
      return res.status(200).json(response);
    })
    .catch((err) => {
      if (!err.statusCode) err.statusCode = 500;
      next(err);
    });
}
