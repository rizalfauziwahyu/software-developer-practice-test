const userServices = require('../services/user.service');
const { validationResult } = require('express-validator');
const Role = require('../_helpers/profile');
const apiResponse = require('../middleware/apiresponse');

module.exports = {
  login,
  refreshToken,
  revokeToken,
};

function login(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error('Validation Failed!');
    error.statusCode = 422;
    error.data = errors.array();
    return next(error);
  }

  const { username, password } = req.body;
  const ipAddress = req.ip;

  userServices
    .authenticate({ username, password, ipAddress })
    .then(({ refreshToken, profile, token }) => {
      setTokenCookie(res, refreshToken);
      const response = apiResponse.getResponse(true, 'login success', {
        profile,
        token,
      });
      return res.status(200).json(response);
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
}

function refreshToken(req, res, next) {
  const token = req.cookies.refreshToken;
  const ipAddress = req.ip;
  userServices
    .refreshToken({ token, ipAddress })
    .then(({ refreshToken, ...user }) => {
      user.refreshToken = refreshToken;
      setTokenCookie(res, refreshToken);
      const response = apiResponse.getResponse(
        true,
        'Refresh token success!',
        user
      );
      return res.status(200).json(response);
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
}

function revokeToken(req, res, next) {
  // accept token from request body or cookie
  const token = req.cookies.refreshToken;
  const ipAddress = req.ip;

  if (!token) {
    const error = new Error('Unauthorized user');
    error.statusCode = 401;
    throw error;
  }

  // users can revoke their own tokens and admins can revoke any tokens
  if (!req.user.ownsToken(token) && req.user.role !== Role.Admin) {
    const error = new Error('Unauthorized user');
    error.statusCode = 401;
    throw error;
  }

  userServices
    .revokeToken({ token, ipAddress })
    .then(() => res.status(200).json({ message: 'logout success' }))
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
}

// Helper function

function setTokenCookie(res, token) {
  // create http only cookie with refresh token that expires in 7 days
  const cookieOptions = {
    httpOnly: true,
    expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000),
  };
  res.cookie('refreshToken', token, cookieOptions);
}
