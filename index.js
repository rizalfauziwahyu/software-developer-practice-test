const path = require('path');
const fs = require('fs');

const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');
const csrf = require('csurf');
const compression = require('compression');
const morgan = require('morgan');

const { errorHandler } = require('./middleware/apiresponse');

const app = express();

app.set('views', 'views');
app.set('view engine', 'ejs');

const accessLogStream = fs.createWriteStream(
  path.join(__dirname, 'access.log'),
  { flags: 'a' }
);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: '4MB' }));
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization, csrf-token, Content-Disposition'
  );
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PUT, PATCH, DELETE'
  );

  next();
});

app.use(cookieParser());
// app.use(csrf({ cookie: true }));
app.use(helmet());
app.use(compression());
app.use(morgan('combined', { stream: accessLogStream }));

const authRoutes = require('./routes/v1/auth.route');
const activityRoutes = require('./routes/v1/activity.route');
const activitiesRoutes = require('./routes/v1/activities.route');
const accountRoutes = require('./routes/v1/account.route');
const skillsRoutes = require('./routes/v1/skills.route');

app.use('/v1', require('./routes/v1/csrf.route'));
app.use('/v1/auth', authRoutes);
app.use('/v1/activity', activityRoutes);
app.use('/v1/activities', activitiesRoutes);
app.use('/v1/user', accountRoutes);
app.use('/v1/skills', skillsRoutes);

app.use(errorHandler);

const seeds = require('./_helpers/seeds');
seeds();

console.log(process.env.NODE_ENV);

const port =
  process.env.NODE_ENV === 'production' ? process.env.PORT || 80 : 5000;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
