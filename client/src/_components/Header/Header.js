import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <nav className='navbar navbar-light bg-light'>
      <div className='container justify-content-between'>
        <Link className='navbar-brand' to='/'>
          Training App
        </Link>
        <Link to='/login'>Logout</Link>
      </div>
    </nav>
  );
};

export default Header;
