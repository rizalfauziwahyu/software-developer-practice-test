import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { skillActions } from '../../_actions/skill.actions';
import { activityActions } from '../../_actions/activity.actions';

import ActivityCard from './ActivityCard/ActivityCard';
import ActivityCreate from './ActivityCard/ActivityCreate';
import UserCreate from '../User/UserCreate';

const Activities = () => {
  const skills = useSelector((state) => state.skills);
  const activities = useSelector((state) => state.activities);
  const authentication = useSelector((state) => state.authentication);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(skillActions.getAll());
  }, []);

  const handleSkillChange = (e) => {
    dispatch(activityActions.getBySkillId(e.target.value));
  };

  return (
    <div className='container py-4'>
      <div className='row justify-content-start'>
        <div className='col-md-6'>
          <div className='form-group'>
            <label htmlFor='skillsSelector'>List of skills</label>
            <select
              onChange={(event) => handleSkillChange(event)}
              className='form-control'
              defaultValue={skills.items ? skills.items[0].skill_id : ''}
              id='skillsSelector'>
              {skills.loading && <option value=''>Loading skills...</option>}
              <option value=''>Select skill</option>
              {skills.items &&
                skills.items.map((skill, index) => {
                  return (
                    <option key={index} value={skill._id}>
                      {skill.skill_name}
                    </option>
                  );
                })}
            </select>
          </div>
          {activities.loading && <h5>Loading..</h5>}
          {activities.items &&
            activities.items.map((activity) => {
              return (
                <ActivityCard
                  id={activity._id}
                  key={activity._id}
                  title={activity.title}
                  description={activity.description}
                  startdate={activity.startdate}
                  enddate={activity.enddate}
                  skill_name={activity.skill_name}
                />
              );
            })}
        </div>
        <div className='col-md-6'>
          {authentication.user.profile === 'expert' ? <ActivityCreate /> : ''}
          {authentication.user.profile === 'board' ? <UserCreate /> : ''}
        </div>
      </div>
    </div>
  );
};

export default Activities;
