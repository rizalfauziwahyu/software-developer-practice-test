import React, { useState, useEffect } from 'react';
import DatePicker from 'react-datepicker';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import history from '../../../_helpers/history';

import { activityActions } from '../../../_actions/activity.actions';
import { alertActions } from '../../../_actions/alert.actions';

import 'react-datepicker/dist/react-datepicker.css';

const ActivityCreate = () => {
  const [activity, setActivity] = useState({
    title: '',
    description: '',
    startdate: Date.now(),
    enddate: Date.now() + 24 * 60 * 60 * 1000,
    skill: '',
    participants: [],
  });

  const [submitted, setSubmitted] = useState(false);
  const create = useSelector((state) => state.activity.create);
  const skills = useSelector((state) => state.skills);
  const alert = useSelector((state) => state.alert);
  const dispatch = useDispatch();

  useEffect(() => {
    history.listen((location, action) => {
      // clear alert on location change
      dispatch(alertActions.clear());
    });
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setActivity((activity) => ({ ...activity, [name]: value }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(activity.skill);
    setSubmitted(true);
    if (
      activity.title &&
      activity.description &&
      activity.startdate &&
      activity.enddate &&
      activity.skill
    ) {
      dispatch(activityActions.create(activity));
    }
  };
  return (
    <div>
      <div>
        {alert.message && (
          <div className={`alert ${alert.type}`}>{alert.message}</div>
        )}
      </div>
      <h4>Register an Activity</h4>
      <div className='form-group'>
        <label htmlFor='skillsSelector'>Choose a skill</label>
        <select
          name='skill'
          onChange={handleChange}
          className='form-control'
          defaultValue={skills.items ? skills.items[0].skill_id : ''}
          id='skillsSelector'>
          {skills.loading && <option value=''>Loading skills...</option>}
          <option value=''>Select skill</option>
          {skills.items &&
            skills.items.map((skill, index) => {
              return (
                <option key={index} value={skill.skill_name}>
                  {skill.skill_name}
                </option>
              );
            })}
        </select>
        {submitted && !activity.skill && (
          <div className='invalid-feedback'>Skill is required</div>
        )}
      </div>
      <form name='form' onSubmit={handleSubmit}>
        <div className='form-group'>
          <label>Title</label>
          <input
            type='text'
            name='title'
            value={activity.title}
            onChange={handleChange}
            className={
              'form-control' +
              (submitted && !activity.title ? ' is-invalid' : '')
            }
          />
          {submitted && !activity.title && (
            <div className='invalid-feedback'>Title is required</div>
          )}
        </div>
        <div className='form-group'>
          <label>Description</label>
          <textarea
            name='description'
            value={activity.description}
            onChange={handleChange}
            className={
              'form-control' +
              (submitted && !activity.description ? ' is-invalid' : '')
            }>
            {' '}
          </textarea>
          {submitted && !activity.description && (
            <div className='invalid-feedback'>Description is required</div>
          )}
        </div>
        <div className='row'>
          <div className='col-md-6'>
            <div className='form-group'>
              <label>Start date</label>
              <div className=''>
                <DatePicker
                  className='form-control'
                  selected={activity.startdate}
                  onChange={(date) => {
                    setActivity((activity) => ({
                      ...activity,
                      startdate: date,
                    }));
                  }}
                />
              </div>

              {submitted && !activity.startdate && (
                <div className='invalid-feedback'>Start date is required</div>
              )}
            </div>
          </div>
          <div className='col-md-6'>
            <div className='form-group'>
              <label>End date</label>
              <div className=''>
                <DatePicker
                  className='form-control'
                  selected={activity.enddate}
                  onChange={(date) => {
                    if (date < activity.startdate) return;
                    setActivity((activity) => ({
                      ...activity,
                      enddate: date,
                    }));
                  }}
                />
              </div>

              {submitted && !activity.startdate && (
                <div className='invalid-feedback'>End date is required</div>
              )}
            </div>
          </div>
        </div>

        <div className='form-group'>
          <button className='btn btn-primary'>
            {create && (
              <span className='spinner-border spinner-border-sm mr-1'></span>
            )}
            Create
          </button>
        </div>
      </form>
    </div>
  );
};

export default ActivityCreate;
