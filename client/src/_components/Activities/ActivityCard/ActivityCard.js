import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { activityActions } from '../../../_actions/activity.actions';
import ActivityUpdate from './ActivityUpdate';
import { Modal, Button } from 'react-bootstrap';

const ActivityCard = ({
  id,
  title,
  description,
  startdate,
  enddate,
  participants,
  skill_name,
}) => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const user = useSelector((state) => state.authentication.user);
  const dispatch = useDispatch();

  const handleDelete = () => {
    dispatch(activityActions.destroy(id));
  };

  return (
    <div className='card mb-4'>
      <div className='card-body'>
        <h5 className='card-title'>{title}</h5>
        <hr />
        <h6 className='card-subtitle mb-2'>{skill_name}</h6>
        <h6 className='card-subtitle mb-2 text-muted'>
          {startdate} - {enddate}
        </h6>
        <p className='card-text'>{description}</p>
      </div>
      {user.profile === 'expert' && (
        <div className='card-body'>
          <button
            type='button'
            onClick={handleShow}
            className='btn btn-primary mr-2 '
            data-toggle='modal'
            data-target={`#${id}`}>
            Edit
          </button>
          <button onClick={handleDelete} className='btn btn-danger'>
            Delete
          </button>
        </div>
      )}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ActivityUpdate
            id={id}
            title={title}
            description={description}
            startdate={startdate}
            enddate={enddate}
            skill={skill_name}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default ActivityCard;
