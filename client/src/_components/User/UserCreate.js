import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { userActions } from '../../_actions/user.actions';

function UserCreate() {
  const [user, setUser] = useState({
    name: '',
    email: '',
    username: '',
    password: '',
    profile: 'trainer',
    skill: '',
  });
  const loading = useSelector((state) => state.user);
  const skills = useSelector((state) => state.skills);
  const alert = useSelector((state) => state.alert);
  const [submitted, setSubmitted] = useState(false);
  const dispatch = useDispatch();

  function handleChange(e) {
    const { name, value } = e.target;
    setUser((user) => ({ ...user, [name]: value }));
  }

  function handleSubmit(e) {
    e.preventDefault();

    setSubmitted(true);
    if (user.name && user.email && user.username && user.password) {
      dispatch(userActions.register(user));
    }
  }

  return (
    <div>
      <h2>Register an Trainer</h2>
      <form name='form' onSubmit={handleSubmit}>
        <div className='form-group'>
          <label>Name</label>
          <input
            type='text'
            name='name'
            value={user.name}
            onChange={handleChange}
            className={
              'form-control' + (submitted && !user.name ? ' is-invalid' : '')
            }
          />
          {submitted && !user.name && (
            <div className='invalid-feedback'>Name is required</div>
          )}
        </div>
        <div className='form-group'>
          <label>Email</label>
          <input
            type='email'
            name='email'
            value={user.email}
            onChange={handleChange}
            className={
              'form-control' + (submitted && !user.email ? ' is-invalid' : '')
            }
          />
          {submitted && !user.email && (
            <div className='invalid-feedback'>Email is required</div>
          )}
        </div>
        <div className='form-group'>
          <label>Username</label>
          <input
            type='text'
            name='username'
            value={user.username}
            onChange={handleChange}
            className={
              'form-control' +
              (submitted && !user.username ? ' is-invalid' : '')
            }
          />
          {submitted && !user.username && (
            <div className='invalid-feedback'>Username is required</div>
          )}
        </div>
        <div className='form-group'>
          <label>Password</label>
          <input
            type='password'
            name='password'
            value={user.password}
            onChange={handleChange}
            className={
              'form-control' +
              (submitted && !user.password ? ' is-invalid' : '')
            }
          />
          {submitted && !user.password && (
            <div className='invalid-feedback'>Password is required</div>
          )}
        </div>
        <div className='form-group'>
          <label>Choose a profile</label>
          <select name='profile' onChange={handleChange} className='form-control'>
              <option value=''>Select profile</option>
            <option value='trainer'>Trainer</option>
            <option value='expert'>Expert</option>
          </select>
        </div>
        <div className='form-group'>
          <label>Choose a skill</label>
          <select name='skill' onChange={handleChange} className='form-control'>
            {skills.loading && <option value=''>Loading skills...</option>}
            <option value=''>Select skill</option>
            {skills.items &&
              skills.items.map((skill, index) => {
                return (
                  <option key={index} value={skill.skill_name}>
                    {skill.skill_name}
                  </option>
                );
              })}
          </select>
        </div>
        <div className='form-group'>
          <button className='btn btn-primary'>Register</button>
        </div>
      </form>
    </div>
  );
}

export default UserCreate;
