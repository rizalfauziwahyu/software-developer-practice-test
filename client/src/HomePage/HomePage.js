import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Header from '../_components/Header/Header';
import Activities from '../_components/Activities/Activities';

function HomePage() {
  return (
    <div className='homepage'>
      <Header />
      <Activities />
    </div>
  );
}

export { HomePage };
