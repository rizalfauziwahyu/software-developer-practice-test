import { skillConstants } from '../_constants/skill.constants';
import { skillService } from '../_services/skill.service';
import { alertActions } from './alert.actions';

const getAll = () => {
  return (dispatch) => {
    dispatch(request());

    skillService.getAll().then(
      (skills) => {
        dispatch(success(skills));
      },
      (error) => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };

  function request() {
    return { type: skillConstants.GETALL_REQUEST };
  }
  function success(skills) {
    return { type: skillConstants.GETALL_SUCCESS, skills };
  }
  function failure(error) {
    return { type: skillConstants.GETALL_FAILURE, error };
  }
};

export const skillActions = {
  getAll,
};
