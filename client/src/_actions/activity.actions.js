import { activityConstants } from '../_constants/activity.constants';
import { activityService } from '../_services/activity.service';
import { alertActions } from './alert.actions';

const getBySkillId = (skillId) => {
  return (dispatch) => {
    dispatch(request());

    activityService.getBySkillId(skillId).then(
      (activities) => {
        dispatch(success(activities));
      },
      (error) => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };

  function request() {
    return { type: activityConstants.GETALL_REQUEST };
  }
  function success(activities) {
    return { type: activityConstants.GETALL_SUCCESS, activities };
  }
  function failure(error) {
    return { type: activityConstants.GETALL_FAILURE, error };
  }
};

const create = (activity) => {
  return (dispatch) => {
    dispatch(request());
    activityService.create(activity).then(
      () => {
        dispatch(success());
        dispatch(alertActions.success('create success'));
      },
      (error) => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };

  function request() {
    return { type: activityConstants.CREATE_REQUEST };
  }
  function success() {
    return { type: activityConstants.CREATE_SUCCESS };
  }
  function failure(error) {
    return { type: activityConstants.CREATE_FAILURE, error };
  }
};

const update = (activityId, activity) => {
  return (dispatch) => {
    dispatch(request());

    activityService
      .update(activityId, activity)
      .then(
        (data) => {
          dispatch(success());
          dispatch(alertActions.success('udpate success'));
        },
        (error) => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() {
    return { type: activityConstants.UPDATE_REQUEST };
  }
  function success() {
    return { type: activityConstants.UPDATE_SUCCESS };
  }
  function failure(error) {
    return { type: activityConstants.UPDATE_FAILURE, error };
  }
};

const destroy = (activityId) => {
  return (dispatch) => {
    dispatch(request());

    activityService.destroy(activityId).then(
      (data) => {
        dispatch(success());
        dispatch(alertActions.success('delete success'));
      },
      (error) => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };

  function request() {
    return { type: activityConstants.UPDATE_REQUEST };
  }
  function success() {
    return { type: activityConstants.UPDATE_SUCCESS };
  }
  function failure(error) {
    return { type: activityConstants.UPDATE_FAILURE, error };
  }
};

export const activityActions = {
  getBySkillId,
  create,
  update,
  destroy,
};
