import { skillConstants } from '../_constants/skill.constants';

export function skills(state = {}, action) {
  switch (action.type) {
    case skillConstants.GETALL_REQUEST:
      return {
        loading: true,
      };
    case skillConstants.GETALL_SUCCESS:
      return {
        items: action.skills,
      };
    case skillConstants.GETALL_FAILURE:
      return {
        error: action.error,
      };
    default:
      return state;
  }
}
