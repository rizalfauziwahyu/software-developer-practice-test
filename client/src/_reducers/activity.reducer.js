import { activityConstants } from '../_constants/activity.constants';

const activity = (state = {}, action) => {
  switch (action.type) {
    case activityConstants.CREATE_REQUEST:
      return {
        loading: true,
      };
    case activityConstants.CREATE_SUCCESS:
      return {};
    case activityConstants.CREATE_FAILURE:
      return {
        error: action.error,
      };
    case activityConstants.UPDATE_REQUEST:
      return {
        loading: true,
      };
    case activityConstants.UPDATE_SUCCESS:
      return {};
    case activityConstants.UPDATE_FAILURE:
      return {
        error: action.error,
      };
    case activityConstants.DELETE_REQUEST:
      return {
        loading: true,
      };
    case activityConstants.DELETE_SUCCESS:
      return {};
    case activityConstants.DELETE_FAILURE:
      return {
        error: action.error,
      };
    default:
      return {};
  }
};

const activities = (state = {}, action) => {
  switch (action.type) {
    case activityConstants.GETALL_REQUEST:
      return {
        loading: true,
      };
    case activityConstants.GETALL_SUCCESS:
      return {
        items: action.activities,
      };
    case activityConstants.GETALL_FAILURE:
      return {
        error: action.error,
      };
    default:
      return state;
  }
};

export { activities, activity };
