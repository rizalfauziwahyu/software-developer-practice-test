import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { alert } from '../_reducers/alert.reducer';
import { authentication } from '../_reducers/authentication.reducer';
import { registration } from '../_reducers/registration.reducer';
import { users } from '../_reducers/users.reducer';
import { skills } from '../_reducers/skill.reducer';
import { activity, activities } from '../_reducers/activity.reducer';

const rootReducer = combineReducers({
  alert,
  authentication,
  registration,
  users,
  skills,
  activity,
  activities,
});

const loggerMiddleware = createLogger();

export const store = createStore(
  rootReducer,
  applyMiddleware(thunkMiddleware, loggerMiddleware)
);
