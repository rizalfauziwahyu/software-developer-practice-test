import { config } from '../_helpers/config';
import { authHeader } from '../_helpers/auth-header';
import { activity } from '../_reducers/activity.reducer';

const getBySkillId = (skillId) => {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };

  return fetch(`${config.apiUrl}/v1/activities/${skillId}`, requestOptions)
    .then(handleResponse)
    .then((activities) => activities);
};

const create = (activity) => {
  const requestOptions = {
    method: 'POST',
    headers: Object.assign(
      {},
      {
        'Content-Type': 'application/json',
      },
      authHeader()
    ),
    body: JSON.stringify(activity),
  };

  return fetch(`${config.apiUrl}/v1/activity`, requestOptions)
    .then(handleResponse)
    .then((data) => data);
};

const update = (activityId, activity) => {
  const requestOptions = {
    method: 'PUT',
    headers: Object.assign(
      {},
      {
        'Content-Type': 'application/json',
      },
      authHeader()
    ),
    body: JSON.stringify(activity),
  };

  return fetch(
    `${config.apiUrl}/v1/activity/${activityId}`,
    requestOptions
  ).then(handleResponse);
};

const destroy = (activityId) => {
  const requestOptions = {
    method: 'DELETE',
    headers: Object.assign(
      {},
      {
        'Content-Type': 'application/json',
      },
      authHeader()
    ),
  };

  return fetch(
    `${config.apiUrl}/v1/activity/${activityId}`,
    requestOptions
  ).then(handleResponse);
};

const handleResponse = (response) => {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        // location.reload(true);
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data.data;
  });
};

export const activityService = {
  getBySkillId,
  create,
  update,
  destroy,
};
