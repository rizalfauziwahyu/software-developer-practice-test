import { config } from '../_helpers/config';
import { authHeader } from '../_helpers/auth-header';

const getAll = () => {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };

  return fetch(`${config.apiUrl}/v1/skills`, requestOptions)
    .then(handleResponse)
    .then((skills) => skills);
};

const handleResponse = (response) => {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        // location.reload(true);
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data.data;
  });
};

export const skillService = {
  getAll,
};
