import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';

import { history } from './_helpers/history';
import { PrivateRoute } from './_components/PrivateRoute';
import { LoginPage } from './LoginPage/LoginPage';
import { HomePage } from './HomePage/HomePage';

function App() {
  return (
    <Router>
      <Switch>
        <PrivateRoute exact path='/' component={HomePage} />
        <Route path='/login' component={LoginPage} />
        <Redirect from='*' to='/' />
      </Switch>
    </Router>
  );
}

export default App;
