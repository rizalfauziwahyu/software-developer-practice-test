const db = require('../_helpers/db');

module.exports = {
  getSkills,
  create,
};

async function getSkills() {
  const skills = await db.Skill.find({}, '-__v');
  return skills;
}

async function create({ skill_id, skill_name }) {
  const skill = new db.Skill({
    skill_id,
    skill_name,
  });

  await skill.save();
}
