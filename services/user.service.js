const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const db = require('../_helpers/db');
const Role = require('../_helpers/profile');

module.exports = {
  authenticate,
  register,
  create,
  update,
  updateRoleProfile,
  _delete,
  refreshToken,
  revokeToken,
  getAll,
  getById,
  getUser,
  getRefreshTokens,
  getRefreshToken,
};

async function authenticate({ username, password, ipAddress }) {
  const user = await db.User.findOne({ username: username });

  if (!user || !bcrypt.compareSync(password, user.password)) {
    const error = new Error('invalid login');
    error.statusCode = 401;
    throw error;
  }

  // authentication successful so generate jwt and refresh tokens
  const jwtToken = generateJwtToken(user);
  const refreshToken = generateRefreshToken(user, ipAddress);

  // save refresh token
  await refreshToken.save();

  // return basic details and tokens
  return {
    refreshToken: refreshToken.token,
    profile: user.profile,
    token: jwtToken,
  };
}

async function register(params) {
  // validate
  if (
    await db.User.findOne({ email: params.email, username: params.username })
  ) {
    // send already registered error in email to prevent account enumeration
    const error = new Error('Data cannot be processed');
    error.statusCode = 422;
    throw error;
  }

  // create account object
  const user = new db.User(params);

  // hash password
  user.password = bcrypt.hashSync(params.password);

  // save account
  await user.save();
}

async function refreshToken({ token, ipAddress }) {
  const refreshToken = await getRefreshToken(token);
  const user = await db.User.findById(refreshToken.user);

  // replace old refresh token with a new one and save
  const newRefreshToken = generateRefreshToken(user, ipAddress);
  console.log(newRefreshToken.token);
  refreshToken.revoked = Date.now();
  refreshToken.revokedByIp = ipAddress;
  refreshToken.replacedByToken = newRefreshToken.token;
  await refreshToken.save();
  await newRefreshToken.save();

  // generate new jwt
  const jwtToken = generateJwtToken(user);

  // return basic details and token
  return {
    ...basicDetails(user),
    jwtToken,
    refreshToken: newRefreshToken.token,
  };
}

async function revokeToken({ token, ipAddress }) {
  const refreshToken = await getRefreshToken(token);
  // revoke token and save
  refreshToken.revoked = Date.now();
  refreshToken.revokedByIp = ipAddress;
  await refreshToken.save();
}

async function create(params) {
  // validate
  if (await db.User.findOne({ email: params.email })) {
    const error = new Error(`Email ${paramas.email} is already registerd`);
    error.statusCode = 400;
    throw error;
  }

  const user = new db.User(params);
  user.verified = Date.now();

  // hash password
  user.password = bcrypt.hashSync(params.password);

  // save account
  await user.save();

  return basicDetails(user);
}

async function update(id, params) {
  const user = await getUser(id);

  // validate
  if (
    user.email !== params.email &&
    (await db.User.findOne({ email: params.email }))
  ) {
    const error = new Error(`Email ${params.email} is already taken`);
    error.statusCode = 400;
    throw error;
  }

  // hash password if it was entered
  if (params.password) {
    params.password = bcrypt.hash(params.password);
  }

  // copy params to account and save
  Object.assign(user, params);
  user.updatedAt = Date.now();
  await user.save();

  return basicDetails(user);
}

async function updateRoleProfile(user, params) {
  switch (user.role) {
    case Role.Student:
      const student = await db.Student.findOne({ userId: user.id });
      if (!student) {
        const error = new Error('Student not found!');
        error.statusCode = 422;
        throw error;
      }
      Object.assign(student, params);
      return await student.save();
    case Role.Teacher:
      const teacher = await db.Teacher.findOne({ userId: user.id });
      if (!teacher) {
        const error = new Error('Teacher not found!');
        error.statusCode = 422;
        throw error;
      }
      Object.assign(teacher, params);
      return await teacher.save();
    default:
      return {};
  }
}

async function _delete(id) {
  const user = await getUser(id);
  await user.remove();
}

async function getAll(user) {
  let profile;
  switch (user.role) {
    case Role.Student:
      profile = await getUser(user.id);
      const student = await db.Student.findOne({ userId: user.id });
      if (!student) {
        const error = new Error('You not registered as student!');
        error.statusCode = 422;
        throw error;
      }
      Object.assign(profile, student);
      return profile;
    case Role.Teacher:
      profile = basicDetails(await getUser(user.id));
      const teacher = basicTeacherDetails(
        await db.Teacher.findOne({ userId: user.id })
      );
      if (!teacher) {
        const error = new Error('You not registered as teacher!');
        error.statusCode = 422;
        throw error;
      }
      return Object.assign({}, profile, teacher);

    case Role.Admin:
      const users = await db.User.find();
      return users.map((x) => basicDetails(x));
  }
}

async function getById(id) {
  const user = await getUser(id);
  return basicDetails(user);
}

async function getRefreshTokens(userId) {
  // check that user exists
  await getUser(userId);

  // return refresh token for user
  const refreshToken = await db.RefreshToken.find({ user: userId });
  return refreshToken;
}

async function getRefreshToken(token) {
  const refreshToken = await db.RefreshToken.findOne({ token: token });
  if (!refreshToken || !refreshToken.isActive) {
    const error = new Error('Unauthorized user');
    error.statusCode = 401;
    throw error;
  }
  return refreshToken;
}

async function getUser(id) {
  if (!db.isValidId(id)) {
    const error = new Error('User not found!');
    error.statusCode = 400;
    throw error;
  }

  const user = await db.User.findById(id);
  if (!user) {
    const error = new Error('User not found!');
    error.statusCode = 400;
    throw error;
  }

  return user;
}

// helper function
function generateJwtToken(user) {
  // create a jwt token containing the user id that expires in 15 minutes
  return jwt.sign({ sub: user.id, id: user.id }, process.env.JWT_SECRET, {
    expiresIn: '1d',
  });
}

function generateRefreshToken(user, ipAddress) {
  // create a refresh token that expires in 7 days
  return new db.RefreshToken({
    user: user,
    token: randomTokenString(),
    expires: new Date(Date.now() + 7 * 24 * 60 * 1000),
    createdByIp: ipAddress,
  });
}

function randomTokenString() {
  return crypto.randomBytes(40).toString('hex');
}

function basicDetails(user) {
  const { id, email, name, username, profile, skill } = user;
  return { id, email, name, username, profile, skill };
}
