const db = require('../_helpers/db');

module.exports = {
  getActivity,
  getActivities,
  create,
  update,
  destroy,
};

async function getActivity(id) {
  const activity = db.TrainingActivity.findById(id);
  if (!activity) {
    const error = new Error('Data cannot be processed');
    error.statusCode = 422;
    throw error;
  }

  return activity;
}

async function getActivities(skill_id) {
  const skill = await db.Skill.findById(skill_id);
  const activities = await db.TrainingActivity.find({
    skill_name: skill.skill_name,
  })
    .sort({ startdate: 1 })
    .populate('participants.userId', 'name profile skill');
  return activities;
}

async function create({
  skill,
  title,
  description,
  startdate,
  enddate,
  participants,
}) {
  const skillObject = await db.Skill.findOne({ skill_name: skill });

  if (!skillObject) {
    const error = new Error('Data cannot be processed');
    error.statusCode = 422;
    throw error;
  }

  const activity = new db.TrainingActivity({
    skill_id: skillObject.skill_id,
    skill_name: skillObject.skill_name,
    title,
    description,
    startdate,
    enddate,
    participants,
  });
  await activity.save();
}

async function update(
  id,
  { skill, title, description, startdate, enddate, participants }
) {
  const activity = await getActivity(id);

  const skillObject = await db.Skill.findOne({ skill_name: skill });
  if (!skillObject) {
    const error = new Error('Data cannot be processed');
    error.statusCode = 422;
    throw error;
  }

  Object.assign(activity, {
    skill_id: skillObject.skill_id,
    skill_name: skillObject.skill_name,
    title,
    description,
    startdate: startdate.toString(),
    enddate: enddate.toString(),
    participants,
  });
  await activity.save();
}

async function destroy(id) {
  const activity = await getActivity(id);
  await activity.remove();
}
